const bcrypt = require('bcrypt');

class UsersService {
  constructor() {
    this.users = [];
  }

  async registerUser(user) {
    const exist = await this.findUserByEmail(user.email);
    if (exist) {
      throw new Error('User already exists!');
    }

    user.password = await bcrypt.hash(user.password, 10);

    this.users.push(user);
  }

  async findUserByEmail(email) {
    return this.users.find(({ email: currentUserEmail }) => email === currentUserEmail);
  }

  async getUsers() {
    return this.users;
  }
}

module.exports = UsersService;
